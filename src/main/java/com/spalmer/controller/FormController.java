package com.spalmer.controller;

import com.spalmer.model.FormDTO;
import com.spalmer.service.FormService;
import com.spalmer.user.model.response.ResponseDTO;
import com.spalmer.user.model.response.ResponseDTO.ResponseDTOBuilder;
import com.spalmer.user.model.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;
import java.util.List;

@RestController
public class FormController {

	private FormService formService;

	@Autowired
	public FormController(final FormService formService) {
		this.formService = formService;
	}

	@SuppressWarnings("unchecked")
	@RequestMapping("/{formName}")
	public ResponseDTO getForm(Principal principal, @PathVariable String formName) {
		User user = (User) ((UsernamePasswordAuthenticationToken) principal).getPrincipal();

		List<FormDTO> dtos = formService.getForms(user, formName);

		return new ResponseDTOBuilder<List<FormDTO>>()
				.success(Boolean.TRUE)
				.response(dtos)
				.build();
	}
}
