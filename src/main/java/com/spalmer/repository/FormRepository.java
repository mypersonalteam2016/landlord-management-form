package com.spalmer.repository;

import com.spalmer.model.Form;
import com.spalmer.user.model.user.User;
import org.springframework.data.mongodb.repository.MongoRepository;

import org.springframework.data.querydsl.QueryDslPredicateExecutor;


import java.util.List;

public interface FormRepository extends MongoRepository<Form,String>, QueryDslPredicateExecutor<Form> {


}
