package com.spalmer.service;

import com.spalmer.model.Form;
import com.spalmer.model.FormDTO;
import com.spalmer.user.model.user.User;

import java.util.List;

public interface FormService {


	List<FormDTO> getForms(User user, String formName);
}
