package com.spalmer.service;

import com.querydsl.core.types.Predicate;
import com.spalmer.model.Form;
import com.spalmer.model.FormDTO;
import com.spalmer.model.QForm;
import com.spalmer.repository.FormRepository;
import com.spalmer.user.model.authority.Authority;
import com.spalmer.user.model.user.User;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;


@Component
public class FormServiceImpl implements FormService {

	private FormRepository formRepository;
	private ModelMapper modelMapper;

	@Autowired
	public FormServiceImpl(final FormRepository formRepository, final ModelMapper modelMapper) {
		this.formRepository = formRepository;
		this.modelMapper = modelMapper;
	}

	@Override
	public List<FormDTO> getForms(User user, String formName) {
		Predicate predicate = getPredicate(user, formName);

		Iterable<Form> forms = formRepository.findAll(predicate);

		return StreamSupport.stream(forms.spliterator(), false)
				.map(this::mapToDTO)
				.collect(Collectors.toList());
	}

	private Predicate getPredicate(User user, String formName) {
		QForm form = QForm.form;

		List<String> authorities = user.getAuthorities().stream()
				.map(Authority::getAuthority)
				.collect(Collectors.toList());

		return (form.userId.eq(user.getId())
				.or(form.userId.isNull()))
				.and(form.formName.eq(formName))
				.and(form.authority.in(authorities));
	}

	private FormDTO mapToDTO(Form form) {
		return modelMapper.map(form, FormDTO.class);
	}



}
